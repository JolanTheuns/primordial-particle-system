import pygame
import numpy as np


class Particle:
    def __init__(self, x, y, orientation, v):
        self.x = x
        self.y = y
        self.orientation = orientation
        self.v = 0.67
        self.a = 180
        self.b = 17
        self.r = 5
        self.deltaPhi = 0
        
# Angle rotation a
# Radius r
# Number of other particles in r is Nt
# Number of particles in righ half of r is Rt
# Number of particles in left half of r is Lt
# Number of particles determine color in visualisation
# beta models a rotation proportional to local neighborhood size
# a + beta*nt*(Rt-Lt)
# a = 180, b=17, v=0.67, r=5

    def calculate(self, Nt, Rt, Lt):
        self.deltaPhi = self.a + self.b * Nt * np.sign(Rt-Lt)

    def moveParticle(self):
        # Calculate location it needs to go
        return self

    def color(self, Nt):
        return self
        # More neigbours equals brighter color
