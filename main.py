import time
import numpy as np
import pygame

pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((500, 500))
pygame.display.set_caption("Primordial Particle Systems")
clock = pygame.time.Clock()
FPS = 60

running = True
while running:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
